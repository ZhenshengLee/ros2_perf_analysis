#!/bin/bash
#
# Call this script with an executable argument to trace it

lttng create test --output=./ros-trace/

lttng enable-event -c testchan -u lttng_ust_statedump:start
lttng enable-event -c testchan -u lttng_ust_statedump:end
lttng enable-event -c testchan -u lttng_ust_statedump:bin_info
lttng enable-event -c testchan -u lttng_ust_statedump:build_id

lttng enable-event -c testchan -u lttng_ust_cyg_profile_fast:func_entry
lttng enable-event -c testchan -u lttng_ust_cyg_profile_fast:func_exit

lttng add-context -u -t vpid -t vtid -t procname -t ip
lttng enable-event -c kernel -k sched_switch

lttng start

LD_PRELOAD=/usr/lib/x86_64-linux-gnu/liblttng-ust-cyg-profile-fast.so $*

lttng stop
lttng destroy
